import {existsSync, readFileSync} from 'fs'
import {basename, join} from 'path'

export const packageName = (): string => {
  let name = ''

  const packageJsonPath = join(process.cwd(), 'package.json')
  if (existsSync(packageJsonPath)) {
    const data = readFileSync(packageJsonPath)
    const json = JSON.parse(data.toString())

    name = json.name
  } else {
    name = basename(process.cwd())
  }

  return name
}
