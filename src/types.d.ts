type PluginConfiguration = [
  string,
  {[key: string]: unknown}?,
]

type PluginConfigurator = () => PluginConfiguration | null

type SemanticReleaseChangelogPluginConfiguration = [
  '@semantic-release/changelog', {
    changelogTitle?: string,
  }?,
]

type SemanticReleaseCommitAnalyzerPluginConfiguration = [
  '@semantic-release/commit-analyzer', {
    preset?: string,
    presetConfig?: {[key: string]: unknown},
  }?,
]

type SemanticReleaseGitPluginConfiguration = [
  '@semantic-release/git', {
    message?: string,
  }?,
]

type SemanticReleaseGithubPluginConfiguration = [
  '@semantic-release/github',
]

type SemanticReleaseGitlabPluginConfiguration = [
  '@semantic-release/gitlab',
]

type SemanticReleaseNpmPluginConfiguration = [
  '@semantic-release/npm',
]

type SemanticReleaseReleaseNotesGeneratorPluginConfiguration = [
  '@semantic-release/release-notes-generator', {
    preset?: string,
  }?,
]
