import {
  configure as configureSemanticReleaseChangelog,
} from './plugins/semantic-release-changelog'
import {
  configure as configureSemanticReleaseCommitAnalyzer,
} from './plugins/semantic-release-commit-analyzer'
import {
  configure as configureSemanticReleaseGit,
} from './plugins/semantic-release-git'
import {
  configure as configureSemanticReleaseGithub,
} from './plugins/semantic-release-github'
import {
  configure as configureSemanticReleaseGitlab,
} from './plugins/semantic-release-gitlab'
import {
  configure as configureSemanticReleaseNpm,
} from './plugins/semantic-release-npm'
import {
  configure as configureSemanticReleaseReleaseNotesGenerator,
} from './plugins/semantic-release-release-notes-generator'

const configurators: PluginConfigurator[] = [
  configureSemanticReleaseCommitAnalyzer,
  configureSemanticReleaseReleaseNotesGenerator,
  configureSemanticReleaseChangelog,
  configureSemanticReleaseNpm,
  configureSemanticReleaseGit,
  configureSemanticReleaseGithub,
  configureSemanticReleaseGitlab,
]

const plugins: PluginConfiguration[] = []

for (const configure of configurators) {
  const config = configure()
  if (config !== null) {
    plugins.push(config)
  }
}

export = {
  plugins,
}
