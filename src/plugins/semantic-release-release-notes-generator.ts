export const configure = (): SemanticReleaseReleaseNotesGeneratorPluginConfiguration => {
  return [
    '@semantic-release/release-notes-generator', {
      preset: 'conventionalcommits',
    },
  ]
}
