export const configure = (): SemanticReleaseGithubPluginConfiguration | null => {
  const {
    GITHUB_ACTIONS,
  } = process.env

  if (GITHUB_ACTIONS === undefined) {
    return null
  }

  return [
    '@semantic-release/github',
  ]
}
