import {execFileSync} from 'child_process'

import {packageName} from '../util'


export const configure = (): SemanticReleaseGitPluginConfiguration | null => {
  const {
    GIT_GPG_KEY_ID,
    GIT_GPG_KEYS,
  } = process.env

  if (GIT_GPG_KEY_ID === undefined || GIT_GPG_KEYS === undefined) {
    return null
  }

  execFileSync('gpg', ['--import'], {
    input: Buffer.from(GIT_GPG_KEYS, 'base64').toString('ascii'),
  })

  execFileSync('git', ['config', 'commit.gpgsign', 'true'])
  execFileSync('git', ['config', 'user.signingkey', GIT_GPG_KEY_ID])

  return [
    '@semantic-release/git', {
      message: `chore(release): Release ${packageName()} v\${nextRelease.version}`,
    },
  ]
}
