export const configure = (): SemanticReleaseChangelogPluginConfiguration => {
  return [
    '@semantic-release/changelog', {
      changelogTitle: '# Changelog',
    },
  ]
}
