import {packageName} from '../util'

export const configure = (): SemanticReleaseCommitAnalyzerPluginConfiguration => {
  return  [
    '@semantic-release/commit-analyzer', {
      preset: 'conventionalcommits',
      presetConfig: {
        releaseCommitMessageFormat: `chore(release): Release ${packageName()} {{currentTag}}`,
      },
    },
  ]
}
