export const configure = (): SemanticReleaseGitlabPluginConfiguration | null => {
  const {
    GITLAB_CI,
  } = process.env

  if (GITLAB_CI === undefined) {
    return null
  }

  return [
    '@semantic-release/gitlab',
  ]
}
