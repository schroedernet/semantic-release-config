export const configure = (): SemanticReleaseNpmPluginConfiguration | null => {
  const {
    NPM_TOKEN,
  } = process.env

  if (NPM_TOKEN === undefined) {
    return  null
  }

  return [
    '@semantic-release/npm',
  ]
}
