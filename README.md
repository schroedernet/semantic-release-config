# @schroedernet/semantic-release-config <!-- omit in toc -->

Opinionated configuration for [semantic-release][semantic-release-github].

- [Overview](#overview)
- [Usage](#usage)
- [Installation](#installation)
- [Configuration](#configuration)
  - [GitHub](#github)
  - [GitLab](#gitlab)
  - [Git](#git)
  - [Npm](#npm)
  - [Project name](#project-name)
- [Security considerations](#security-considerations)
  - [Access tokens](#access-tokens)
  - [Hosting sites](#hosting-sites)
  - [GPG keys](#gpg-keys)


## Overview

This configuration aims to support best practices by keeping a changelog with
signed commits alongside source code, and to "just work" for simple projects
based on the environment and project conventions that provide the context in
which it is run.


## Usage

When the configuration is first loaded by semantic-release, the exact list of
plugins to be used is determined by the environment (variables).  If a plugin
requires configuration or only makes sense in certain contexts, it is not loaded
unless relevant environment variables are present.  This makes it possible to
mix and match plugins with a single configuration, potentially making code more
portable, particularly with regard to hosting sites, CI/CD, and mirroring.

For example, it is possible to have a project set up to be hosted on gitlab.com,
where packaging and changelog updating occurs, mirrored to both github.com and a
private GitLab instance, and still have "releases" created at all three
locations.  I.e.:

- gitlab.com
  - Canonical "source of truth"
  - Updates changelog file
  - Packages code, e.g. for npm
  - Creates "release" on gitlab.com with most recent changes in description
- github.com
  - Mirror
  - Creates "release" on github.com with most recent changes in description
- Private GitLab instance
  - Mirror
  - Creates "release" on private GitLab instance with most recent changes in
    description

See [Configuration][local-config] for a full list of supported plugins.


## Installation

1. Install
   [`@schroedernet/semantic-release-config`][schroedernet-semantic-release-config-npm]
   as development dependency
   - `npm install -D @schroedernet/semantic-release-config`
2. Add to [`extends`][semantic-release-github-docs-config-extends] array in
   `.releaserc`
   - `{extends: ['@schroedernet/semantic-release-config']}`


## Configuration

The full list of supported plugins (in order of possible execution), of which a
subset will run (determined at run-time based on environment configuration) is:

1. [`@semantic-release/commit-analyzer`][semantic-release-commit-analyzer-github]
   - Analyze commits with conventional-changelog
   - Required; always loaded
2. [`@semantic-release/release-notes-generator`][semantic-release-release-notes-generator-github]
   - Generate changelog content with conventional-changelog
   - Required; always loaded
3. [`@semantic-release/changelog`][semantic-release-changelog-github]
   - Create or update a changelog file
   - Required; always loaded
4. [`@semantic-release/npm`][semantic-release-npm-github]
   - Publish a npm package
   - Conditional; see [detailed description][local-config-npm]
5. [`@semantic-release/git`][semantic-release-git-github]
   - Commit release assets to the project's git repository
   - Conditional; see [detailed description][local-config-git]
6. [`@semantic-release/github`][semantic-release-github-github]
   - Publish a GitHub release and comment on released Pull Requests/Issues
   - Conditional; see [detailed description][local-config-github]
7. [`@semantic-release/gitlab`][semantic-release-gitlab-github]
   - Publish a GitLab release
   - Conditional; see [detailed description][local-config-gitlab]


### GitHub

The [`@semantic-release/github`][semantic-release-github-github] plugin will be
loaded automatically when run in the context of GitHub Actions.

Required environment variables:

- `GITHUB_ACTIONS`
  - Set by GitHub Actions to let programs know they are running in GitHub
    Actions
- `GITHUB_TOKEN`/`GH_TOKEN`
  - OAuth token for interacting with GitHub's Releases API
  - The simplest method is to re-use the `GITHUB_TOKEN` variable provided by the
    GitHub Actions environment if you are fine with releases being created by
    the default `github-actions` user.  This can be done by adding
    `GITHUB_TOKEN: ${{ secrets.GITHUB_TOKEN }}` to the `env` key of the release
    step in the worflow yaml file.
  - Alternatively, a different OAuth token with `repo` scope can be provided to
    create the release with a custom user.  It is recommended to use the
    `GH_TOKEN` variable for this so as to avoid conflict with the reserved
    `GITHUB_` namespace for environment variables provided by GitHub Actions.


### GitLab

The [`@semantic-release/gitlab`][semantic-release-gitlab-github] plugin will be
loaded automatically when run in the context of GitLab CI.

Required environment variables:

- `GITLAB_CI`
  - Set by GitLab CI to let programs know they are running in GitLab CI
- `GITLAB_TOKEN`/`GL_TOKEN`
  - OAuth token for interacting with GitLab's Releases API
  - A personal access token with `api` scope is required.  Alternatively, a
    project access token may be used.


### Git

The [`@semantic-release/git`][semantic-release-git-github] plugin will be loaded
when the required environment variables are set.

Required environment variables:

- `GIT_GPG_KEY_ID`
  - ID of the GPG key to use for signing commits
  - Must be available in the keys provided by `GIT_GPG_KEYS`
- `GIT_GPG_KEYS`
  - Exported public _and private_ GPG keys to use for signing commits
  - Must be base64 encoded so GitLab can properly mask the value
  - Example export command:
    - `echo "$(gpg --armor --export && gpg --armor --export-secret-keys)" |
      base64 -w 0`
  - See [Secutiy considerations][local-security]
- `GITHUB_TOKEN`/`GH_TOKEN`/`GITLAB_TOKEN`
  - OAuth token with push access to the repository
  - This is the same as what semantic-release already requires for
    [authentication][semantic-release-github-docs-config-auth] to create git
    tags.

Optional environment variables:

- `GIT_AUTHOR_NAME`
  - The author name associated with the release commit
  - See [Git environment variables][git-book-env-vars-committing]
- `GIT_AUTHOR_EMAIL`
  - The author email associated with the release commit
  - See [Git environment variables][git-book-env-vars-committing]
- `GIT_COMMITER_NAME`
  - The commiter name associated with the release commit
  - See [Git environment variables][git-book-env-vars-committing]
- `GIT_COMMITER_EMAIL`
  - The commiter email associated with the release commit
  - See [Git environment variables][git-book-env-vars-committing]


### Npm

The [`@semantic-release/npm`][semantic-release-npm-github] plugin will be loaded
when the required environment variables are set.

Required environment variables:

- `NPM_TOKEN`
  - Access token for interacting with npm
  - The token should be of "automation" type


### Project name

The project name is used to configure how commit messages are analyzed by the
[`@semantic-release/commit-analyzer`][semantic-release-commit-analyzer-github]
plugin and how commit messages are created by the
[`@semantic-release/git`][semantic-release-git-github] plugin.

If a `package.json` file exists in the project root, the value of the `name` key
from that file is used.  Otherwise, the name of the project directory is used.


## Security considerations

**Disclaimer:**  The following are recommended _considerations_.  There is no
"one size fits all" solution to security and it is important that appropriate
consideration also be given to the context of the individual problem to be
solved.


### Access tokens

_Using a "bot" account_ (as opposed to an account designated to a human) for
automation tasks reduces risk by limiting the capabilities of potential
attackers in the event the secret is exposed.  This also has the benefit of
separating people from infrastructure.

_Limiting the scope of access tokens_ to cover only required permissions reduces
risk by limiting the capabilities of potential attackers in the event the secret
is exposed.  See [Configuration][local-config] details for what scopes are
relevant and when.

_Avoiding token re-use_ reduces risk both by limiting the capabilities of
potential attackers in the event the secret is exposed and by limiting exposure
to a particular secret.  This also has the benefit of simplifying the process of
revoking and/or updating a particular token by limiting the number of places it
is used.


### Hosting sites

_Limiting project collaborator permissions_ to only what is required reduces
risk by limiting secret exposure.  This also has the benefit of reducing
attack/abuse potential in the event a collaborator's credentials are abused.

_Using protected branches/tags/etc_ reduces risk by limiting secret exposure.
This also has the benefit of keeping potentially untested or unreviewed code
away from potentially sensitive APIs.


### GPG keys

_Using subkeys_ reduces risk by limiting (master) secret exposure.  This also
has the benefit of being able to keep the master key offline and more strongly
secured since the master key is only required for creating or modifying the
subkey.

_Avoiding key re-use_ reduces risk by limiting secret exposure.  This also has
the benefit of simplifying the process of revoking and/or updating a particular
key by limiting the number of places it is used, and is made easier by the fact
that any given key can produce an unlimited number of subkeys.

_Using ECC over RSA_ has possible advantages such as reduced key and signature
length.  It also has the possible benefit of offering implementation
designs by more "independent" entities.


<!-- Links -->

<!-- -- local -->

[local-config]:
#configuration
"Configuration"

[local-config-git]:
#git
"Configuration / Git"

[local-config-github]:
#github
"Configuration / GitHub"

[local-config-gitlab]:
#gitlab
"Configuration / GitLab"

[local-config-npm]:
#npm
"Configuration / Npm"

[local-security]:
#security-considerations
"Security considerations"


<!-- -- git -->

[git-book-env-vars-committing]:
https://git-scm.com/book/en/v2/Git-Internals-Environment-Variables#_committing
"Environment variables / Commiting (Git book - Internals)"


<!-- -- schroedernet-semantic-release-config -->

[schroedernet-semantic-release-config-npm]:
https://www.npmjs.com/package/@schroedernet/commitlint-config
"@schroedernet/commitlint-config (npm)"


<!-- -- semantic-release -->

[semantic-release-github]:
https://github.com/semantic-release/semantic-release
"semantic-release (GitHub)"

[semantic-release-github-docs-config-auth]:
https://github.com/semantic-release/semantic-release/blob/master/docs/usage/ci-configuration.md#authentication
"semantic-release (GitHub - docs/usage/ci-configuration.md#authentication)"

[semantic-release-github-docs-config-extends]:
https://github.com/semantic-release/semantic-release/blob/master/docs/usage/configuration.md#extends
"semantic-release (GitHub - docs/usage/configuration.md#extends)"


<!-- -- semantic-release-changelog -->

[semantic-release-changelog-github]:
https://github.com/semantic-release/changelog "@semantic-release/changelog
(GitHub)"


<!-- -- semantic-release-commit-analyzer -->

[semantic-release-commit-analyzer-github]:
https://github.com/semantic-release/commit-analyzer
"@semantic-release/commit-analyzer (GitHub)"


<!-- -- semantic-release-git -->

[semantic-release-git-github]:
https://github.com/semantic-release/git
"@semantic-release/git (GitHub)"


<!-- -- semantic-release-github -->

[semantic-release-github-github]:
https://github.com/semantic-release/github
"@semantic-release/github (GitHub)"


<!-- -- semantic-release-gitlab -->

[semantic-release-gitlab-github]:
https://github.com/semantic-release/gitlab
"@semantic-release/gitlab (GitHub)"


<!-- -- semantic-release-npm -->

[semantic-release-npm-github]:
https://github.com/semantic-release/npm
"@semantic-release/npm (GitHub)"


<!-- -- semantic-release-release-notes -->

[semantic-release-release-notes-generator-github]:
https://github.com/semantic-release/release-notes-generator
"@semantic-release/release-notes-generator (GitHub)"
