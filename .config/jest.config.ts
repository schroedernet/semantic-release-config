// https://jestjs.io/docs/en/configuration.html

export default {
  cacheDirectory: 'tmp/tests/cache',
  collectCoverage: true,
  coverageDirectory: 'tmp/tests/reports/coverage',
  coverageProvider: 'v8',
  coverageReporters: ['cobertura', 'lcov', 'text'],
  injectGlobals: false,
  preset: 'ts-jest',
  reporters: [
    'default',
    ['jest-junit', {outputDirectory: 'tmp/tests/reports/unit'}],
  ],
  rootDir: '..',
  roots: ['<rootDir>/tests'],
  setupFilesAfterEnv: ['<rootDir>/tests/helpers/setup.ts'],
  testEnvironment: 'node',
  testRunner: 'jest-circus/runner',
  verbose: true,
}
