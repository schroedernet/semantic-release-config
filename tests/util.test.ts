import {mkdirSync, rmSync, writeFileSync} from 'fs'
import {chdir, cwd} from 'process'

import {afterEach, beforeEach, describe, expect, test} from '@jest/globals'

import {packageName} from '../src/util'


describe('util', () => {
  describe('package.json is present', () => {
    const packageJsonName = 'foobarbaz'

    beforeEach(() => {
      writeFileSync('package.json', `{"name": "${packageJsonName}"}`)
    })

    afterEach(() => {
      rmSync('package.json')
    })

    test('uses name from package.json', () => {
      expect(packageName()).toEqual(packageJsonName)
    })
  })

  describe('package.json is not present', () => {
    const origCwd = cwd()
    const packageDirName = 'barbazfoo'

    beforeEach(() => {
      mkdirSync(packageDirName)
      chdir(packageDirName)
    })

    afterEach(() => {
      chdir(origCwd)
    })

    test('uses cwd name', () => {
      expect(packageName()).toEqual(packageDirName)
    })
  })
})
