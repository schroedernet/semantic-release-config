#! /usr/bin/env node

// @ts-check

const assert = require('assert')
const fs = require('fs');

(() => {
  // check args
  assert.strict.equal(process.argv[2], '--import')

  // THANKS https://stackoverflow.com/q/39801643/12029481
  assert.strict.equal(process.stdin.isTTY, undefined)

  // check stdin
  const data = fs.readFileSync(process.stdin.fd, 'utf8').replace(/\n$/, '')
  assert.strict.equal(data, 'barbaz')

  process.exit(0)
})()
