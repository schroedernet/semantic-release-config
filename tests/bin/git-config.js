#! /usr/bin/env node

// @ts-check

const assert = require('assert');

(() => {
  const errors = []
  let testCount = 0

  try {
    testCount += 1
    assert.strict.equal(process.argv[2], 'config')
    assert.strict.equal(process.argv[3], 'commit.gpgsign')
    assert.strict.equal(process.argv[4], 'true')
  } catch (err) {
    errors.push(err)
  }

  try {
    testCount += 1
    assert.strict.equal(process.argv[2], 'config')
    assert.strict.equal(process.argv[3], 'user.signingkey')
    assert.strict.equal(process.argv[4], 'foo')
  } catch (err) {
    errors.push(err)
  }

  // one and only one is expected to pass at a time
  if (errors.length !== testCount - 1) {
    console.error(errors)
    process.exit(1)
  }

  process.exit(0)
})()
