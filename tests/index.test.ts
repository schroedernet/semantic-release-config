import {describe, expect, test} from '@jest/globals'

import index from '../src/index'


describe('index', () => {
  describe('exports', () => {
    describe('plugins', () => {
      test('non-empty array of configurations', () => {
        expect(index.plugins.length).toBeGreaterThan(0)
      })
    })
  })
})
