import {beforeEach, describe, expect, test} from '@jest/globals'

import {configure} from '../../src/plugins/semantic-release-commit-analyzer'


describe('semantic-release-commit-analyzer', () => {
  describe('configure', () => {
    let config: SemanticReleaseCommitAnalyzerPluginConfiguration

    beforeEach(() => {
      config = configure()
    })

    test('returns package name', () => {
      expect(config[0]).toEqual('@semantic-release/commit-analyzer')
    })

    test('sets preset option to conventionalcommits', () => {
      expect(config[1]?.preset).toEqual('conventionalcommits')
    })

    test('sets presetConfig.releaseCommitMessageFormat option', () => {
      expect(config[1]?.presetConfig?.releaseCommitMessageFormat)
        .not.toBeUndefined()
    })
  })
})
