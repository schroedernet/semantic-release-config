import {afterEach, beforeEach, describe, expect, test} from '@jest/globals'

import {configure} from '../../src/plugins/semantic-release-git'

import {copyBin} from '../helpers/util'


describe('semantic-release-git', () => {
  describe('configure', () => {
    const {GIT_GPG_KEY_ID, GIT_GPG_KEYS} = process.env

    let config: SemanticReleaseGitPluginConfiguration | null

    describe('GIT_GPG_KEY_ID (only) is defined', () => {
      beforeEach(() => {
        process.env.GIT_GPG_KEY_ID = 'foo'
        delete process.env.GIT_GPG_KEYS

        config = configure()
      })

      afterEach(() => {
        process.env.GIT_GPG_KEY_ID = GIT_GPG_KEY_ID
        process.env.GIT_GPG_KEYS = GIT_GPG_KEYS
      })

      test('returns null', () => {
        expect(config).toBeNull()
      })
    })

    describe('GIT_GPG_KEYS (only) is defined', () => {
      beforeEach(() => {
        delete process.env.GIT_GPG_KEY_ID
        process.env.GIT_GPG_KEYS = 'bar'

        config = configure()
      })

      afterEach(() => {
        process.env.GIT_GPG_KEY_ID = GIT_GPG_KEY_ID
        process.env.GIT_GPG_KEYS = GIT_GPG_KEYS
      })

      test('returns null', () => {
        expect(config).toBeNull()
      })
    })

    describe('GIT_GPG_KEY_ID and GIT_GPG_KEYS (both) are defined', () => {
      beforeEach(() => {
        process.env.GIT_GPG_KEY_ID = 'foo'
        process.env.GIT_GPG_KEYS = Buffer.from('barbaz').toString('base64')
      })

      afterEach(() => {
        process.env.GIT_GPG_KEY_ID = GIT_GPG_KEY_ID
        process.env.GIT_GPG_KEYS = GIT_GPG_KEYS
      })

      describe('gpg import', () => {
        beforeEach(() => {
          copyBin('gpg-import.js', 'gpg')
          copyBin('dummy.js', 'git')
        })

        test('called correctly', () => {
          expect(() => {configure()}).not.toThrow()
        })
      })

      describe('git config', () => {
        beforeEach(() => {
          copyBin('git-config.js', 'git')
          copyBin('dummy.js', 'gpg')
        })

        test('called correctly', () => {
          expect(() => {configure()}).not.toThrow()
        })
      })
    })

    describe('return value', () => {
      beforeEach(() => {
        copyBin('dummy.js', 'git')
        copyBin('dummy.js', 'gpg')
        config = configure()
      })

      test('returns package name', () => {
        expect(config![0]).toEqual('@semantic-release/git')
      })

      test('sets message options', () => {
        expect(config![1]?.message).not.toBeUndefined()
      })
    })

    describe('GIT_GPG_KEY_ID and GIT_GPG_KEYS (neither) are not defined', () => {
      beforeEach(() => {
        delete process.env.GIT_GPG_KEY_ID
        delete process.env.GIT_GPG_KEYS

        config = configure()
      })

      afterEach(() => {
        process.env.GIT_GPG_KEY_ID = GIT_GPG_KEY_ID
        process.env.GIT_GPG_KEYS = GIT_GPG_KEYS
      })

      test('returns null', () => {
        expect(config).toBeNull()
      })
    })
  })
})
