import {beforeEach, describe, expect, test, } from '@jest/globals'

import {configure} from '../../src/plugins/semantic-release-changelog'


describe('semantic-release-changelog', () => {
  describe('configure', () => {
    let config: SemanticReleaseChangelogPluginConfiguration

    beforeEach(() => {
      config = configure()
    })

    test('returns package name', () => {
      expect(config[0]).toEqual('@semantic-release/changelog')
    })

    test('sets changelog title', () => {
      expect(config[1]?.changelogTitle).not.toBeUndefined()
    })
  })
})
