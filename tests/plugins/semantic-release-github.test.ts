import {afterEach, beforeEach, describe, expect, test} from '@jest/globals'

import {configure} from '../../src/plugins/semantic-release-github'


describe('semantic-release-github', () => {
  describe('configure', () => {
    const {GITHUB_ACTIONS} = process.env

    describe('GITHUB_ACTIONS defined', () => {
      let config: SemanticReleaseGithubPluginConfiguration | null

      beforeEach(() => {
        process.env.GITHUB_ACTIONS = 'true'

        config = configure()
      })

      afterEach(() => {
        process.env.GITHUB_ACTIONS = GITHUB_ACTIONS
      })

      test('returns package name', () => {
        expect(config![0]).toEqual('@semantic-release/github')
      })
    })

    describe('GITHUB_ACTIONS not defined', () => {
      let config: SemanticReleaseGithubPluginConfiguration | null

      beforeEach(() => {
        delete process.env.GITHUB_ACTIONS

        config = configure()
      })

      afterEach(() => {
        process.env.GITHUB_ACTIONS = GITHUB_ACTIONS
      })

      test('returns null', () => {
        expect(config).toBeNull()
      })
    })
  })
})
