import {afterEach, beforeEach, describe, expect, test} from '@jest/globals'

import {configure} from '../../src/plugins/semantic-release-npm'


describe('semantic-release-npm', () => {
  describe('configure', () => {
    const {NPM_TOKEN} = process.env

    describe('NPM_TOKEN defined', () => {
      let config: SemanticReleaseNpmPluginConfiguration | null

      beforeEach(() => {
        process.env.NPM_TOKEN = 'foobarbaz'

        config = configure()
      })

      afterEach(() => {
        process.env.NPM_TOKEN = NPM_TOKEN
      })

      test('returns package name', () => {
        expect(config![0]).toEqual('@semantic-release/npm')
      })
    })

    describe('NPM_TOKEN not defined', () => {
      let config: SemanticReleaseNpmPluginConfiguration | null

      beforeEach(() => {
        delete process.env.NPM_TOKEN

        config = configure()
      })

      afterEach(() => {
        process.env.NPM_TOKEN = NPM_TOKEN
      })

      test('returns null', () => {
        expect(config).toBeNull()
      })
    })
  })
})
