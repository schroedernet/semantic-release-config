import {beforeEach, describe, expect, test} from '@jest/globals'

import {configure} from '../../src/plugins/semantic-release-release-notes-generator'


describe('semantic-release-release-notes-generator', () => {
  describe('configure', () => {
    let config: SemanticReleaseReleaseNotesGeneratorPluginConfiguration

    beforeEach(() => {
      config = configure()
    })

    test('returns package name', () => {
      expect(config[0]).toEqual('@semantic-release/release-notes-generator')
    })

    test('sets preset option to conventionalcommits', () => {
      expect(config[1]?.preset).toEqual('conventionalcommits')
    })
  })
})
