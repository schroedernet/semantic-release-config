import {afterEach, beforeEach, describe, expect, test} from '@jest/globals'

import {configure} from '../../src/plugins/semantic-release-gitlab'


describe('semantic-release-gitlab', () => {
  describe('configure', () => {
    const {GITLAB_CI} = process.env

    describe('GITLAB_CI defined', () => {
      let config: SemanticReleaseGitlabPluginConfiguration | null

      beforeEach(() => {
        process.env.GITLAB_CI = 'true'

        config = configure()
      })

      afterEach(() => {
        process.env.GITLAB_CI = GITLAB_CI
      })

      test('returns package name', () => {
        expect(config![0]).toEqual('@semantic-release/gitlab')
      })
    })

    describe('GITLAB_CI not defined', () => {
      let config: SemanticReleaseGitlabPluginConfiguration | null

      beforeEach(() => {
        delete process.env.GITLAB_CI

        config = configure()
      })

      afterEach(() => {
        process.env.GITLAB_CI = GITLAB_CI
      })

      test('returns null', () => {
        expect(config).toBeNull()
      })
    })
  })
})
