import {PathLike, copyFileSync, mkdirSync, readdirSync, rmSync} from 'fs'
import {join} from 'path'
import {chdir} from 'process'


const TMP_DIR_NAME = 'tmp'
const TESTS_DIR_NAME = 'tests'
const WORK_DIR_NAME = 'work'
const BIN_DIR_NAME = 'bin'

const tmpPath = join(process.cwd(), TMP_DIR_NAME)
const tmpTestsPath = join(tmpPath, TESTS_DIR_NAME)
const tmpTestsWorkPath = join (tmpTestsPath, WORK_DIR_NAME)
const tmpTestsBinPath = join(tmpTestsPath, BIN_DIR_NAME)

const testsPath = join(process.cwd(), TESTS_DIR_NAME)
const testsBinPath = join(testsPath, BIN_DIR_NAME)


const cleanDir = (path: PathLike) => {
  for (const file of readdirSync(path)) {
    rmSync(join(path.toString(), file), {force: true, recursive: true})
  }
}


export const cdWorkDir = () => {
  chdir(tmpTestsWorkPath)
}

export const cleanBinDir = () => {
  cleanDir(tmpTestsBinPath)
}

export const cleanWorkDir = () => {
  cleanDir(tmpTestsWorkPath)
}

export const copyBin = (fromFileName: string, toFileName: string) => {
  copyFileSync(
    join(testsBinPath, fromFileName),
    join(tmpTestsBinPath, toFileName),
  )
}

export const createBinDir = () => {
  mkdirSync(tmpTestsBinPath, {recursive: true})
}

export const createWorkDir = () => {
  mkdirSync(tmpTestsWorkPath, {recursive: true})
}
