import {chdir, cwd} from 'process'

import {afterAll, beforeAll, beforeEach} from '@jest/globals'

import {
  cdWorkDir, cleanBinDir, cleanWorkDir, createBinDir, createWorkDir,
} from './util'


const origCwd = cwd()


beforeAll(() => {
  createWorkDir()
  cdWorkDir()

  createBinDir()
})

beforeEach(() => {
  cleanWorkDir()
  cleanBinDir()
})

afterAll(() => {
  chdir(origCwd)
})
